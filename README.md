# Overview
The active phototube base is an alternative to the passive phototube base. When the active function is "OFF", the base functions exactly like a a standard phototube base. However, when "ON" it prevents photoelectrons from multiplying up the dynode chain, effectively turning the tube off. Critically, the device isn't really off, as the capacitors that buffer the dynodes are fully charged and every upper dynode is at the approximate correct potential. Instead, we apply a negative (with respect to the photocathode) voltage that repels photoelectrons and prevents their multiplication up the tube. This prevents saturation in the event of many photoelectrons.

# Implementation
The first prototype has been built. This repo contains the design files and design documentation. KiCAD 5.1.6 was used to design the board. It can be downloaded [here](https://www.kicad.org/)

## Device Requirements
The gate pin on the R878 phototube should switch from "ON" (-10V) to "OFF" (100V) in 1uS using a TTL (0-5V) input signal. All voltages are with respect to the photocathode, unless otherwise specified.

## Device I/O
The phototube is implemented similarly to the passive tube base, with a few caveats. The first major difference is in output -- the output signal runs on the same line (Red SHV connector) as the high voltage input. The signal is recovered externally by a capacitive blocker, using the HV splitter/blocker box Zach built. The other input (Black BNC) is used for the TTL input signal that controls the state of the phototube base. The base was designed to be used with 1200V DC, positive polarity.

![base](base.jpg)

## Circuit details
R1-R12 form a voltage divider. This distributes the high voltage across the dynodes, anode, cathode and gate pin. There also exists C1,C2, and C3 which buffer the higher dynodes. This is all thus far identical to the passive base. However, the active base includes a push-pull pair, common emitter amplifier and Zener diode. The Zener D1 sets the negative output voltage, and is easily replaced if -D1/2 is insufficient. Q3 inverts and translates the TTL input into an acceptable voltage for the base of Q1 and Q2. Q1 and Q2 form a push pull pair to quickly charge and discharge the capacitance built into the gate pin. R16, R17, R19 and R22-R26 form the bias chain to get the emitters of Q1 and Q2 to the correct voltage. C1 is a smoothing capacitor for D1, and R27 keeps the active function disabled when there is no input.

See schematic.pdf for the complete design. It also contains links to some datasheets you may find useful!

### Further Improvements
Q1 is a very high voltage NPN BJT, with a V_ce of 1200V. I chose to tap directly off the main high voltage line in order to not put additional load on the higher dynodes, which I though might cause some irregularities at the node voltage, screwing up the multiplication ratio. However, it might be possible to get away with tapping the voltage divider lower down and simply  aggressively decoupling, allowing for using the tube at higher voltage.

D1 is adjustable. If -10V turns out to be too low, it's trivial to replace with a higher voltage Zener. Be aware that this will require a higher voltage out of the power supply for the dynodes to see the same potential.

Q1 and Q2 have limited hfe. While I designed the circuit to meet the requirements with the lowest specified hfe, it's not a controlled characteristic and more would be better. If the current into the base of Q1/Q2 is too low, the push pull stage will not operate correctly. If this is the case, it's easy to divide the bias chain for Q1/Q2 by a factor of 1.5 or 2 and get additional  base current. However, too much base current will overload the supply and resistor power dissipation, so it's a balancing act.

The 1k resistors on the emitter of Q1 and Q2 control the rise and fall times of the switch. Lower values result in faster switching, to some extent.

### Errata
In v1 of the boards (this repo), C11 needs to be rated for the maximum base operating voltage (1200V). The resistors that feed it also need to be chosen such that the time constant (RC=tau) is not so slow that the voltage arcs over one of the resistors as it charges up the capacitors. I found 1mS worked fine, 100mS did not.

## Simulation
The circuit was simulated with this [software](https://www.falstad.com/circuit/). Included in the repo is text file you can plug in to see values, experiment with different configurations, and explore the circuit.

# Testing
As a first note, these voltages are firmly in the stop-your-heart territory, and the supply is more than capable of delivering enough current, long enough, to make it happen.

Care is required.

Perhaps the most critical aspects of testing the device are the quick rise/fall times with a changing TTL input, long term dynode voltage stability, and a lack of arcing/mechanical degradation.

Ideally, the device will switch "ON" and "OFF" in less than 1uS. If it does not, there are a few levers to pull. First, a lack of base current to Q1/Q2. Simply divide all the base current resistors by 1.5 or 2 and see if performance improves. The second option is adjusting Q1 and Q2. Transistors with higher beta, especially for Q1, exist. However, doing so will require tapping the voltage divider lower down, as nothing I found has a higher V_ce that the STN0214. Obviously, this will require more decoupling and testing. A final option is to try and reduce the capacitance of the gate node. This is unlikely to help unless it's measurably worse than a few pF. An out-of-scope option is to try a push pull MOSFET stage, but that would require some redesign.

Ideally, the device won't age a day -- the resistors will stay stable and the voltages will not drift. However, age heat, humidity, etc. all play a role in shifting values. The device needs to be powered on for a few weeks continuously and dynode voltages tested consistently throughout. A few % shift is OK, larger than that is indicative of problems. Fixing this is a matter of component choice -- ceramic dielectric types, carbon vs thick film, thin film, etc.

Finally, the device cannot arc, at all. Fixing this is either a layout change or using potting compound, or potentially both. It is quite difficult to diagnose and test a potted circuit, so this should be a last step.

# Final Thoughts.

Overall, I think the design is promising and all my initial tests has gone well. It's worked as simulated at some lower voltages and survived at 1200V, which is a great start. I hope this documentation helps you understand the circuity, and I'm happy to answer any questions about it.

